package data_structures;

public class MaxColaPrioridad <T extends Comparable<T>>
{
	private T[] cp;
	private int N;
	private int ndado;
	
	public MaxColaPrioridad(int n)
	{
		ndado=n;
		cp = (T[]) new Comparable[n+1];
		N=0;
	}
	public boolean isEmpty()
	{
		return N==0;
	}
	
	public int size()
	{
		return N;
	}
	
	public void insert(T dato)
	{
		cp[++N] = dato;
		swim(N);
	}
	
	public T delMax()
	{
		T max = cp[1];
		exch(1,N--);
		cp[N+1] = null;
		sink(1);
		return max;
	}
	
	
	private boolean less (int i, int j)
	{
		return cp[i].compareTo(cp[j]) < 0;
	}
	
	private void exch(int i, int j)
	{
		T temp = cp[i];
		cp[i] = cp[j];
		cp[j] = temp;
	}
	
	private void swim(int k)
	{
		while(k > 1 && less(k/2,k))
		{
			exch(k/2,k);
			k = k/2;
		}
	}
	
	private void sink(int i)
	{
		while(i*2 <= N)
		{
			int j = 2*i;
			if(j<N && less(j,j+1))
				j++;
			if(!less(i,j))
				break;
			exch(i,j);
			i=j;
		}
	}
	
	public void clear() {
		cp = (T[]) new Comparable[ndado+1];
		N=0;
	}
}
