package data_structures;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import vo.LocationVO;

public class MaxHeapCP <T extends Comparable<T>> {
	
	
    private T[] cola;                    
    private int cantidadElementos;                     
    private Comparator<T> comparator; 
    int ndado=0;


    public MaxHeapCP (int capacidad) {
    	ndado=capacidad;
        cola = (T[]) new Comparable[capacidad + 1];
        cantidadElementos = 0;
    }

   
    public boolean Estavacia() {
        return cantidadElementos == 0;
    }


    public int darTamano() {
        return cantidadElementos;
    }

    public T darMax() {
        if (Estavacia()) throw new NoSuchElementException();
        return cola[1];
    }

    private void resize(int capacity) {
        assert capacity > cantidadElementos;
        T[] temp = (T[]) new Comparable[capacity];
        for (int i = 1; i <= cantidadElementos; i++) {
            temp[i] = cola[i];
        }
        cola = temp;
    }

    public void insert(T x) {
        if (cantidadElementos == cola.length - 1) {
        	resize(2 * cola.length);
        }
        cola[++cantidadElementos] = x;
        swim(cantidadElementos);
    }
    
    private void swim(int k) {
        while (k > 1 && less(k/2, k)) {
            exch(k, k/2);
            k = k/2;
        }
    }

    /**
     * Removes and returns a largest key on this priority queue.
     *
     * @return a largest key on this priority queue
     * @throws NoSuchElementException if this priority queue is empty
     */
    public T delMax() {
        if (Estavacia()) throw new NoSuchElementException();
        T RTA = cola[1];
        exch(1, cantidadElementos--);
        int k=1;
        while (2*k <= cantidadElementos) {
            int j = 2*k;
            if (j < cantidadElementos && less(j, j+1)) j++;
            if (!less(k, j)) break;
            exch(k, j);
            k = j;
        }
        cola[cantidadElementos+1] = null;  
        if ((cantidadElementos > 0) && (cantidadElementos == (cola.length - 1) / 4)) resize(cola.length / 2);
        
        return RTA;
    }

    private boolean less(int i, int j) {
        if (comparator == null) {
            return ((Comparable<T>) cola[i]).compareTo(cola[j]) < 0;
        }
        else {
            return comparator.compare(cola[i], cola[j]) < 0;
        }
    }

    private void exch(int i, int j) {
        T cambio = cola[i];
        cola[i] = cola[j];
        cola[j] = cambio;
    }
    
    public void clear() {
		cola = (T[]) new Comparable[ndado+1];
		cantidadElementos=0;
	}

}
