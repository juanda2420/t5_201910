package data_structures;

public interface IMaxColaprioridad <T> extends Comparable<T>  {

	public boolean isEmpty();
	public int size();
	public void insert(T dato);
	public T delMax();
	public boolean less (int i, int j);
	public void exch(int i, int j);
	public void swim(int k);
	public void sink(int i);
	
	
}
