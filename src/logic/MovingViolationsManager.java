package logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

import data_structures.Queue;
import vo.LocationVO;
import vo.VOMovingViolations;
;

public class MovingViolationsManager 
{
	
	private Queue<VOMovingViolations> listaQueue  = new Queue<VOMovingViolations>(); 
	
	private Queue<LocationVO> listaLocation = new Queue<LocationVO>();
		
	private LinkedList<LocationVO> listaLinked = new LinkedList<LocationVO>();

	public void loadJanuary(String archivoJanuary) 
	{

		File archivo = new File(archivoJanuary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				ArrayList<String> datosActual = new ArrayList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}
				
				
				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));
				
				LocationVO nuevoLocation = new LocationVO(datosActual.get(3),datosActual.get(2), datosActual.get(13));
				
				listaLocation.enqueue(nuevoLocation);
				listaQueue.enqueue(nuevoViolations);
				listaLinked.add(nuevoLocation);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public void loadFebruary(String archivoFebruary) {

		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				ArrayList<String> datosActual = new ArrayList<String>();

				while (st.hasMoreTokens()) 
				{
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));
				
				LocationVO nuevoLocation = new LocationVO(datosActual.get(3),datosActual.get(2), datosActual.get(13));
				
				listaLocation.enqueue(nuevoLocation);
				listaQueue.enqueue(nuevoViolations);
				listaLinked.add(nuevoLocation);
				
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public void loadMarch(String archivoFebruary) {

		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				ArrayList<String> datosActual = new ArrayList<String>();

				while (st.hasMoreTokens()) 
				{
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));
				
				LocationVO nuevoLocation = new LocationVO(datosActual.get(3),datosActual.get(2), datosActual.get(13));
				
				listaLocation.enqueue(nuevoLocation);
				listaQueue.enqueue(nuevoViolations);
				listaLinked.add(nuevoLocation);
				
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public void loadApril(String archivoFebruary) {

		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				ArrayList<String> datosActual = new ArrayList<String>();

				while (st.hasMoreTokens()) 
				{
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(
						datosActual.get(0), datosActual.get(2), datosActual.get(3), datosActual.get(4),
						datosActual.get(5), datosActual.get(6), datosActual.get(7), datosActual.get(8),
						datosActual.get(9), datosActual.get(10), datosActual.get(11), datosActual.get(12),
						datosActual.get(13), datosActual.get(14), datosActual.get(15));
				
				LocationVO nuevoLocation = new LocationVO(datosActual.get(3),datosActual.get(2), datosActual.get(13));
				
				listaLocation.enqueue(nuevoLocation);
				listaQueue.enqueue(nuevoViolations);
				listaLinked.add(nuevoLocation);

				
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public Queue<VOMovingViolations> darListaMovingViolations()
	{
		return listaQueue;
	}
	
	public Queue<LocationVO> darListaLocationVO()
	{
		return listaLocation;
	}
	
	public LinkedList<LocationVO> darListaLinked()
	{
		return listaLinked;
	}
	
	
}
