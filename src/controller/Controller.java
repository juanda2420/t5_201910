
package controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

import data_structures.IQueue;
import data_structures.MaxColaPrioridad;
import data_structures.MaxHeapCP;
import data_structures.Queue;
import logic.MovingViolationsManager;

import vo.LocationVO;
import vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;
	private IQueue<LocationVO> listLocation;

	MovingViolationsManager manager;

	// Muestra obtenida de los datos cargados
	private LinkedList<LocationVO> muestra = new LinkedList<LocationVO>();
	// Copia de la muestra de datos a ordenar
	private LinkedList<LocationVO> muestraCopia = new LinkedList<LocationVO>();

	private LinkedList<LocationVO> lista = new LinkedList<LocationVO>();
	
	LinkedList<LocationVO> subconjunto = new LinkedList<LocationVO>();

	public Controller() {
		view = new MovingViolationsManagerView();
		manager = new MovingViolationsManager();
		movingViolationsQueue = manager.darListaMovingViolations();
		listLocation = manager.darListaLocationVO();
		lista = manager.darListaLinked();
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		Controller controller = new Controller();

		int nMuestra = 0;
		long startTime;
		long endTime;
		long duration;
		int nDatos = 0;

		MaxColaPrioridad<LocationVO> pruebaMaxCola = new MaxColaPrioridad<LocationVO>(nMuestra);
		MaxHeapCP<LocationVO> pruebaMaxHeap = new MaxHeapCP<LocationVO>(nMuestra);

		while (!fin) {
			view.printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 0:
				controller.loadMovingViolations();
				break;

			case 1:
				// Generar muestra de infracciones a ordenar
				view.printMessage("Dar tamaNo de la muestra: ");
				nMuestra = sc.nextInt();
				muestra = controller.generarMuestra(nMuestra);
				view.printDatosMuestra1(muestra);
				view.printMessage("Muestra generada");
				break;

			case 3:
				if (nMuestra > 0 && muestra != null && muestra.size() == nMuestra) {
					muestraCopia = controller.obtenerCopia(muestra);
					startTime = System.currentTimeMillis();
					pruebaMaxCola = controller.agregarMaxColaPrioridad(muestraCopia);
					;
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMessage("Se agregaron correctamente las infracciones a MaxColaPrioridad");
					view.printMessage("Tiempo: " + duration + " milisegundos");
				} else
					view.printMessage("Muestra Invalida");

				break;

			case 4:
				if (nMuestra > 0 && muestra != null && muestra.size() == nMuestra) {
					muestraCopia = controller.obtenerCopia(muestra);
					startTime = System.currentTimeMillis();
					pruebaMaxHeap = controller.agregarMaxHeapCP(muestraCopia);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMessage("Se agregaron correctamente las infracciones a MaxHeap");
					view.printMessage("Tiempo: " + duration + " milisegundos");
				} else
					view.printMessage("Muestra Invalida");
				break;

			case 5:
				if (nMuestra > 0 && muestra != null && muestra.size() == nMuestra) {
					muestraCopia = controller.obtenerCopia(muestra);
					startTime = System.currentTimeMillis();
					controller.sacarElementosMaxColaPrioridad(pruebaMaxCola);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMessage("Se agregaron correctamente las infracciones a MaxHeap");
					view.printMessage("Tiempo: " + duration + " milisegundos");
				} else
					view.printMessage("Muestra Invalida");
				break;
			case 6:
				if (nMuestra > 0 && muestra != null && muestra.size() == nMuestra) {
					muestraCopia = controller.obtenerCopia(muestra);
					startTime = System.currentTimeMillis();
					controller.sacarElementosMaxHeap(pruebaMaxHeap);
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMessage("Se agregaron correctamente las infracciones a MaxHeap");
					view.printMessage("Tiempo: " + duration + " milisegundos");
				} else
					view.printMessage("Muestra Invalida");
				break;

			case 7:
				if (nMuestra > 0 && muestra != null && muestra.size() == nMuestra) {
					muestraCopia = controller.obtenerCopia(muestra);
					view.printMessage("Ingrese la fecha con hora inicial (Ej : 2018-04-01T11:59:00.000Z)");
					LocalDateTime fechaInicial = convertirFecha_Hora_LDT(sc.next());

					view.printMessage("Ingrese la fecha con hora final (Ej : 2018-04-01T11:59:00.000Z)");
					LocalDateTime fechaFinal = convertirFecha_Hora_LDT(sc.next());

					startTime = System.currentTimeMillis();
					view.printDatosMuestra2(controller.crearMaxColaP(fechaInicial, fechaFinal));
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMessage("Con la cola de prioridad se tomó un tiempo de : " + duration + " milisegundos");

					startTime = System.currentTimeMillis();
					view.printDatosMuestra2(controller.crearMaxHeapCP(fechaInicial, fechaFinal));
					endTime = System.currentTimeMillis();
					duration = endTime - startTime;
					view.printMessage("Con la pila se tomó un tiempo de : " + duration + " milisegundos");

				} else
					view.printMessage("Muestra Invalida");
				break;
			case 8:
				fin = true;
				sc.close();
				break;
			}
		}

	}

	public void loadMovingViolations() {
		manager.loadJanuary("./data/Moving_Violations_Issued_in_January_2018.csv");
		manager.loadFebruary("./data/Moving_Violations_Issued_in_February_2018.csv");
		manager.loadMarch("./data/Moving_Violations_Issued_in_March_2018.csv");
		manager.loadApril("./data/Moving_Violations_Issued_in_April_2018.csv");
	}

	/**
	 * Generar una muestra aleatoria de tamaNo n de los datos leidos. Los datos de
	 * la muestra se obtienen de las infracciones guardadas en la Estructura de
	 * Datos.
	 * 
	 * @param n tamaNo de la muestra, n > 0
	 * @return muestra generada
	 */
	public LinkedList<LocationVO> generarMuestra(int n) {

		if (n <= 0 || n > 350000)
			return null;

		else {
			int contador = 0;
			int aleatorio = 0;

			while (contador != n) {
				aleatorio = (int) ((Math.random()) * n);
				muestra.add(lista.get(aleatorio));
				contador++;
			}

		}

		return muestra;
	}

	public LinkedList<LocationVO> obtenerCopia(LinkedList<LocationVO> muestra) {
		LinkedList<LocationVO> copia = new LinkedList<LocationVO>();
		for (int i = 0; i < muestra.size(); i++) {
			copia.add(muestra.get(i));
		}
		return copia;
	}

	public LinkedList<LocationVO> conteoAddressId(LinkedList<LocationVO> datos) {
		
		LinkedList<LocationVO> RTA = new LinkedList<LocationVO>();

		for (int i = 0; i < datos.size(); i++) {

			LocationVO actual = datos.get(i);

			for (int j = i + 1; j < datos.size(); j++) {

				LocationVO siguiente = datos.get(j);

				if (actual.getAddressId() == siguiente.getAddressId()) {
					int numerosActual = actual.darNumeroDeRegistros();
					actual.actualizarNumberOfRegisters(numerosActual + 1);
					RTA.add(actual);
				}

			}

		}
		return RTA;

	}

	public MaxColaPrioridad<LocationVO> agregarMaxColaPrioridad(LinkedList<LocationVO> datos) {
		MaxColaPrioridad<LocationVO> resp = new MaxColaPrioridad<LocationVO>(datos.size());
		for (LocationVO s : datos) {
			resp.insert(s);
		}
		return resp;
	}

	public MaxHeapCP<LocationVO> agregarMaxHeapCP(LinkedList<LocationVO> datos) {
		MaxHeapCP<LocationVO> resp = new MaxHeapCP<LocationVO>(datos.size());
		for (LocationVO s : datos)
			resp.insert(s);
		return resp;
	}

	public void sacarElementosMaxColaPrioridad(MaxColaPrioridad<LocationVO> datos) {

		int contador = 0;
		while (contador < datos.size()) {
			datos.delMax();
			contador++;
		}

	}

	public void sacarElementosMaxHeap(MaxHeapCP<LocationVO> datos) {
		
		int contador = 0;
		while (contador < datos.darTamano()) {
			datos.delMax();
			contador++;
		}

	}

	public MaxColaPrioridad<LocationVO> crearMaxColaP(LocalDateTime fInicial, LocalDateTime fFinal) {
		MaxColaPrioridad<LocationVO> resp = new MaxColaPrioridad<LocationVO>(listLocation.size());
		for (LocationVO act : listLocation) {
			if ((fInicial.compareTo(convertirFecha_Hora_LDT(act.getDate())) <= 0)
					&& (fFinal.compareTo(convertirFecha_Hora_LDT(act.getDate())) >= 0)) {
				resp.insert(act);
				
			}
		}
		return resp;
	}

	public MaxHeapCP<LocationVO> crearMaxHeapCP(LocalDateTime fInicial, LocalDateTime fFinal) {
		MaxHeapCP<LocationVO> resp = new MaxHeapCP<LocationVO>(listLocation.size());
		for (LocationVO act : listLocation) {
			if ((fInicial.compareTo(convertirFecha_Hora_LDT(act.getDate())) <= 0)
					&& (fFinal.compareTo(convertirFecha_Hora_LDT(act.getDate())) >= 0)) {
				resp.insert(act);
			}
		}
		return resp;
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * 
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa
	 *              para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha) {
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * 
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para
	 *              mes y aaaa para agno, HH para hora, mm para minutos y ss para
	 *              segundos
	 * @return objeto LDT con fecha y hora integrados
	 */

	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora) {
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));

	}
}
