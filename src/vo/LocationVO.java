package vo;


public class LocationVO implements Comparable<LocationVO> 
{
	private int addressId;

	private int numberOfRegisters = 0;

	private String location;
	
	private String date;

	public LocationVO(String pAddressId, String pLocation, String pDate) 
	{
		addressId = Integer.parseInt(pAddressId);
		location = pLocation;
		date = pDate;
	}

	@Override
	public int compareTo(LocationVO o) 
	{
		int RTA = 0;

		if (this.numberOfRegisters < o.numberOfRegisters) 
		{
			RTA = -1;
		} 
		else if (this.numberOfRegisters > o.numberOfRegisters) 
		{
			RTA = 1;
		}
		else 
		{
		

			if (this.location.compareTo(o.location) < 0) 
			{
				RTA = -1;
			} 
			else if (this.location.compareTo(o.location) > 0) 
			{
				RTA = 1;
			} 
			
		}
		return RTA;
	}
	
	public String getLocation()
	{
		return location;
	}
	
	public int darNumeroDeRegistros() 
	{
		return numberOfRegisters;
	}
	
	public int getAddressId()
	{
		return addressId;
	}
	
	public String getDate()
	{
		return date;
	}
	
	public void actualizarNumberOfRegisters(int n)
	{
		numberOfRegisters += n;
	}

}
