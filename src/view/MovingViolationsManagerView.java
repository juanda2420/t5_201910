package view;


import java.util.LinkedList;

import data_structures.MaxColaPrioridad;
import data_structures.MaxHeapCP;
import vo.LocationVO;
import vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el número maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;

	public void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("-------------------Taler 5--------------------");
		System.out.println("0. Cargar las infracciones");
		System.out.println("1. Generar muestra aleatorea de tamano N");
		System.out.println("3. Aplicar metodo agregar a MaxColaPrioridad");
		System.out.println("4. Aplicar metodo agregar a MaxHeapCP");
		System.out.println("5. Aplicar metodo delMax a MaxColaPrioridad");
		System.out.println("6. Aplicar metodo delMax a MaxHeapCP");
		System.out.println("7. Mostrar N vias que tienen la mayor cantidad de infracciones registradas");
		System.out.println("8. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}
	
	public void printDatosMuestra(LinkedList<LocationVO> datos)
	{
		for(LocationVO s : datos)
		{
			System.out.println("number of registers: "+s.darNumeroDeRegistros() + 
					" AddressId: " + s.getAddressId()+ " Location: " +s.getLocation());
		}
	}
	
	public void printDatosMuestra1(LinkedList<LocationVO> datos)
	{
		for(LocationVO s : datos)
		{
			System.out.println("AddressId: " + s.getAddressId()+ " Location: " +s.getLocation());
		}
	}

	public void printDatosMuestra2(MaxColaPrioridad<LocationVO> datos) 
	{
		System.out.println("La cantidad de vías que tiene el mayor número de infracciones registradas son :" + datos.size());
	}
	public void printDatosMuestra2(MaxHeapCP<LocationVO> datos) 
	{
		System.out.println("La cantidad de vías que tiene el mayor número de infracciones registradas son :" + datos.darTamano());
	}
}
