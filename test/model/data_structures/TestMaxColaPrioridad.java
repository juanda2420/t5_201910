package model.data_structures;
import java.util.LinkedList;

import data_structures.MaxColaPrioridad;
import junit.framework.TestCase;
import vo.LocationVO;

public class TestMaxColaPrioridad extends TestCase{


	MaxColaPrioridad<LocationVO> arreglo = new MaxColaPrioridad(20);
	// Muestra obtenida de los datos cargados
	LinkedList<LocationVO> muestra = new LinkedList<LocationVO>();

	/**
	 * Arreglo con los elementos del escenario
	 */
	protected static final String[] ADRESS_ID = { "350", "383", "105", "233", "140", "266", "356", "236", "80", "360",
			"221", "241", "130", "244", "352", "446", "18", "98", "97" };
	protected static final String[] LOCATION = { "hola", "mi", "nombre", "es", "david", "y", "tengo", "parcial", "el",
			"lunes", "de", "lym", "y", "de", "integral", "ayuda", "porfavor", "send", "help", "plz" };
	protected static final String[] DATES = { "a", "b", "c", "d", "e", "f", "g", "h", "i",
			"j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t" };

	public void setupEscenario1() {

		for (int i = 0; i < ADRESS_ID.length; i++) {
			LocationVO nuevo = new LocationVO(ADRESS_ID[i], LOCATION[i], DATES[i]);
			muestra.add(nuevo);
			arreglo.insert(nuevo);
			
		}

	}

	public void testAdd() throws Exception {
		// Prueba la lista vac�a.
		assertTrue("La lista debe estar vacia", arreglo.isEmpty());
		assertEquals("no es 0", 0, arreglo.size());

		// Agrega dos elementos.
		
	
		arreglo.insert(new LocationVO("5", "jeje", "z"));
		arreglo.insert(new LocationVO("7", "fnlrbjale", "y"));
		assertFalse("La lista no deberia estar vacia", arreglo.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, arreglo.size());

		// Agrega 20 elementos.
		arreglo.clear();
		setupEscenario1();

		assertFalse("La lista no es vacia", arreglo.isEmpty());
		assertEquals("La lista debe tener 20 elementos", ADRESS_ID.length, arreglo.size());

	}
	

	public void testDelMax() throws Exception {
		// Prueba la lista vac�a.
		assertTrue("La lista debe estar vacia", arreglo.isEmpty());
		assertEquals("no es 0", 0, arreglo.size());

		// Agrega 20 elementos.
		arreglo.clear();
		setupEscenario1();
		
		LocationVO RTA = arreglo.delMax();
		

		assertEquals("Deberia ser la dirección 266", 266 , RTA.getAddressId());
		assertEquals("Deberia ser la Location  Y","y", RTA.getLocation());
		
		arreglo.clear();
		setupEscenario1();
		
		LocationVO nuevo1  = arreglo.delMax();
		LocationVO nuevo2  = arreglo.delMax();
		LocationVO nuevo3  =arreglo.delMax();
		LocationVO nuevo4  = arreglo.delMax();
		LocationVO nuevo5  = arreglo.delMax();
		LocationVO nuevo6  =arreglo.delMax();
		LocationVO nuevo7  = arreglo.delMax();
		LocationVO nuevo8  = arreglo.delMax();
		LocationVO nuevo9  =arreglo.delMax();
		LocationVO nuevo10  = arreglo.delMax();
		LocationVO nuevo11  = arreglo.delMax();
		LocationVO nuevo12  =arreglo.delMax();
		LocationVO nuevo13  = arreglo.delMax();
		LocationVO nuevo14  = arreglo.delMax();
		LocationVO nuevo15  =arreglo.delMax();
		LocationVO nuevo16  = arreglo.delMax();
		LocationVO nuevo17  = arreglo.delMax();
		LocationVO nuevo18  =arreglo.delMax();
		LocationVO nuevo19  = arreglo.delMax();
	
	
		
		
		assertEquals("La lista debe estar vacia", 0, arreglo.size());
		
		setupEscenario1();
		
		arreglo.delMax();
		arreglo.delMax();
		arreglo.delMax();
		arreglo.delMax();
		LocationVO jeje = arreglo.delMax();
		
		assertEquals("Deberia ser la dirección 446 ", 18 , jeje.getAddressId());
		assertEquals("Deberia ser la Location  porfavor","porfavor", jeje.getLocation());

	}

}
